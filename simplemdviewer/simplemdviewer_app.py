import os
from PySide2.QtGui import QGuiApplication
from PySide2.QtCore import QUrl
from PySide2.QtQml import QQmlApplicationEngine, qmlRegisterType
from simplemdviewer.md_converter import MdConverter

def main():
    """Initializes and manages the application execution"""
    app = QGuiApplication()
    engine = QQmlApplicationEngine()
    
    qmlRegisterType(MdConverter, 'org.mydomain.simplemdviewer',
                    1, 0, 'MdConverter')
 
    base_path = os.path.abspath(os.path.dirname(__file__))
    url = QUrl(f'file://{base_path}/qml/main.qml')
    engine.load(url)

    if len(engine.rootObjects()) == 0:
        quit()

    app.exec_()

if __name__ == "__main__":
    main()
