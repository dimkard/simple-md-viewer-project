from markdown import markdown
from PySide2.QtCore import QObject, Signal, Slot, Property

class MdConverter(QObject):
    """A simple markdown converter"""

    def __init__(self):
        QObject.__init__(self)
        
        self._source_text = ''

    def readSourceText(self):
        return self._source_text

    def setSourceText(self, val):
        self._source_text = val
        self.sourceTextChanged.emit()

    @Signal
    def sourceTextChanged(self):
        pass

    @Slot(result=str)
    def mdFormat(self):
        return markdown(self._source_text)
    
    sourceText = Property(str, readSourceText, setSourceText, notify=sourceTextChanged)

